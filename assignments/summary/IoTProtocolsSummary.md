# IIoT Protocols

**Protocols are the set of rule through which devices communicate with each others.**

### 1. 4-20mA

![](https://www.predig.com/sites/default/files/images/Indicator/back_to_basics/4-20mA_Current_Loops/4-20mA_current_loop_components.jpg)

* The 4-20 mA current loop is the prevailing process control signal in many industries.
* Information through analog signals is transmitted via varying amounts of voltage or current.
* The range of current is taken between 4-20mA because:
    * It is very expensive under 4mA.
    * Signals below 4mA would be unrecognizable.
    * Easier to differentiate a live zero (4mA) signal from a failure in the system (0mA).
* Current input became the preferred and more efficient process control signal.

#### Pros & Cons of 4-20 mA Loops


**Pros:**

* The 4-20 mA current loop is the dominant standard in many industries.
* It is the simplest option to connect and configure.
* It uses less wiring and connections than other signals, greatly reducing initial setup costs.
* Better for traveling long distances, as current does not degrade over long connections like voltage.
* It is less sensitive to background electrical noise.
* Since 4 mA is equal to 0% output, it is incredibly simple to detect a fault in the system.

**Cons:**

* Current loops can only transmit one particular process signal.
* Multiple loops must be created in situations where there are numerous process variables that require transmission. Running so much wire could lead to problems with ground loops if independent loops are not properly isolated.
* These isolation requirements become exponentially more complicated as the number of loops increases.


### 2. Modbus

* Modbus is a communication protocol for use with its programmable logic controllers (PLC).
* Modbus supports communication to and from multiple devices connected to the same cable or Ethernet network.
* The method is used for transmitting information between electronic devices. The device requesting information is called “master” and “slaves” are the devices supplying information. 

### How does Modbus Work?
Modbus is transmitted over serial lines between devices. The simplest setup would be a single serial cable connecting the serial ports on two devices, a Master and a Slave.The data is sent as series of ones and zeroes called bits. Each bit is sent as a voltage. Zeroes are sent as positive voltages and a ones as negative. The bits are sent very quickly. A typical transmission speed is 9600 baud (bits per second).

![](https://realpars.com/wp-content/uploads/2018/12/How-does-Modbus-Communication-Protocol-Work-Between-Devices-1.png)

**_Modbus can be used over 2 interfaces:_**

* RS485 - called as Modbus RTU
* Ethernet - called as Modbus TCP/IP

### RS485

* RS485 is a serial communication method allows multiple devices (up to 32) to communicate.
* RS485 is a serial (like UART) transmission standard, you can put several RS485 devices on the same bus.
* RS485 is not directly compatible: you must use the correct type of interface, or the signals won't go through. Mainly done through an easy to use an RS485 to USB.

### 3. OPCUA

* OPCUA: Open Platform Communications United Architecture.
* OPCUA allows you to communicate with industrial hardware devices(machine to machine) for automation.
* OPCUA is implemented in server/client pairs.
* Client acts as master it decides what to do where Server waits for clients request and reponds accordingly.
* OPCUA protocol is used widely due do its efficient performace,high scalibility and security.
* OPCUA  has protocols that are self reliant and idepentent.They are:Data access(DA),Alarm & Events(AE),Historical Analysis(HDA).
* It is not designed for one platform it can work on any platform.

![](https://www.novotek.com/images/solutionpages/Kepware_solutionpages/2015_OPC_client_server.png)


## Cloud Protocols

### 1. MQTT (Message Queuing Telemetry Transport)

MQTT (MQ Telemetry Transport) is a lightweight messaging protocol that provides resource-constrained network clients with a simple way to distribute telemetry information. The protocol, which uses a publish/subscribe communication pattern, is used for machine-to-machine (M2M) communication and plays an important role in the internet of things (IoT).

* MQTT is publish-subscribe network protocol that helps us transfer messages between devices.
* It is bandwidth-efficient,bidirectional and uses less battery power.(lightweight)
* MQTT broker plays a role of sending/transfering messages to the devices.
* Each device(MQTT client) can subscribe to particular topics(Each slash indicates a topic level).
* When another client publishes a message on a subscribed topic, the broker forwards the message to any client that has subscribed.

#### MQTT Architecture

![](https://www.norwegiancreations.com/wp-content/uploads/2017/07/MQTT-overview.jpg)

MQTT Client as a publisher sends a message to the MQTT broker whose work is to distribute the message accordingly to all other MQTT clients subscribed to the topic on which publisher publishes the message.
Topics are a way to register interest for incoming messages or to specify where to publish the message. Represented by strings, separated by forward slash. Each slash indicates a topic level.


### 2. HTTP (Hypertext Transfer Protocol)

![](http://innovationm.co/wp-content/uploads/2016/10/HTTP-Protocol-624x248.png)

* HTTP:Hyper Text Transfer Protocol.
* It was designed for communication between web browsers and web servers.
* HTTP is a protocol which allows the fetching of resources, such as HTML documents.
* It is a client-server protocol.client sends an HTTP request and Server sends HTTP response.


**A HTTP request consists of:**

* **The request line-** The server could be asked to send the resource to the client.
* **HTTP headers-** These are written on a message to provide the recipient with information about the message, the sender, and the way in which the sender wants to communicate with the recipient.
* **Body-** Content of any HTTP message can be referred to as a message body.


**A HTTP response consists of:**

* **Status line**
* **HTTP header**
* **Message body**

**Requesting Methods:**

* **GET:** Retrieve the resource from the server (e.g. when visiting a page).
* **POST:** Create a resource on the server (e.g. when submitting a form).
* **PUT/PATCH:** Update the resource on the server (used by APIs).
* **DELETE:** Delete the resource from the server (used by APIs).

**_Each protocol uses a particular PORT as a medium for communication so as to control internet traffic._**

* HTTP uses port 80
* HTTPS uses port 443

![](https://gitlab.com/iotiotdotin/project-internship/iotmodule2/-/wikis/uploads/1dcbcc2c7e58e59a042e55b2b2080405/restful-web-services-with-spring-mvc-28-638.jpg)

