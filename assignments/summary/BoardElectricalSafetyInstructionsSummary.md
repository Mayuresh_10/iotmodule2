# Board Electrical Safety instructions

![](https://www.airquipheating.com/Images_Content/Site1/Images/Articles/airquip-safety-first.png)

## Do's and Don'ts

### 1. Power Supply

**Do's:**

* Always make sure that both the output and input voltages of the board are same.
* Before turning on the power supply make sure connections are made properly.

**Don'ts:**

* Do not connect power supply without matching the power rating.
* Never connect a higher output(12V/3Amp) to a lower (5V/2Amp) input .
* Don't connect the wires forcefully into the board.


### 2. Handling

**Do's:**

* Handle devices safely and make sure your hands are dry.
* Unplug appliances before performing any service or repairs on them.
* If the board becomes too hot try to cool it with a external usb fan.
* Keep all electrical circuit contact points enclosed.

**Don'ts**

* Don’t handle the board when its powered ON.
* Never touch electrical equipment when any part of your body is wet.
* Do not touch any sort of metal to the development board.
* While working keep the board on a flat stable surface (wooden table) .
* Do not try to poke, probe, or fix electrical equipment with objects like pencils or rulers because the metal in them can serve as a form of conductor.

### 3. GPIO (Genral Purpose Input output)

A general-purpose input/output (GPIO) is an uncommitted digital signal pin on an integrated circuit or electronic circuit board whose behavior, whether it acts as input or output is controllable by the user at run time.

![](https://camo.githubusercontent.com/1a11eb9f57a3869a9ca3f044d923a1226f2daf9d/68747470733a2f2f7777772e656c656b74726f6e696b2d6b6f6d70656e6469756d2e64652f73697465732f7261737062657272792d70692f666f746f732f7261737062657272792d70692d3135622e6a7067)

**Do's:**

* Find out whether the board runs on 3.3v or 5v logic.
* Always connect the LED (or sensors) using appropriate resistors .
* To Use 5V peripherals with 3.3V use a logic level converter.

**Don'ts:**

* Never connect anything greater than 5v to a 3.3v pin.Don't use high voltages.
* Avoid making connections when the board is running.
* Do not connect a motor directly , use a transistor to drive it.


## Guidelines for using interfaces:

### 1.UART (Universal Asynchronous Receiver Transmitter)

A universal asynchronous receiver-transmitter (UART) is a computer hardware device for asynchronous serial communication in which the data format and transmission speeds are configurable.

![](https://www.circuitbasics.com/wp-content/uploads/2016/01/Introduction-to-UART-Data-Transmission-Diagram.png)

* Connect Rx device1 to Tx device2, similarly Tx device1 to Rx device2.
* If the device1 works on 5v and device2 works at 3.3v then use the level shifting mechanism(voltage divider).
* Genrally UART is used to communicate with board through USB to TTL connection.It doesn't require any protection circuit.
But Senor interfacing using UART require a protection circuit.


### 2. I2C (Inter-Integrated Circuit)
The Inter-Integrated Circuit (I2C) Protocol is a protocol intended to allow multiple "slaves" digital integrated circuits to communicate with one or more "master" chips.

![](https://www.analog.com/-/media/analog/en/landing-pages/technical-articles/i2c-primer-what-is-i2c-part-1-/36684.png?la=en&w=900)

* while using I2C interfaces with sensors SDA and SDL lines must be protected.
* Protection of these lines is done by using pullup registers on both lines.
* If you use the inbuilt pullup registers it doesn't require an external circuit.
* If you are using bread-board to connect your sensor,use the pullup resistor(2.2kohm <= 4K ohm).


### 3. SPI (Serial Peripheral Interface)
Serial Peripheral Interface (SPI) is an interface bus commonly used to send data between microcontrollers and small peripherals such as shift registers, sensors, and SD cards. It uses separate clock and data lines, along with a select line to choose the device you wish to talk to.

![](https://upload.wikimedia.org/wikipedia/commons/thumb/f/fc/SPI_three_slaves.svg/800px-SPI_three_slaves.svg.png)

* SPI in development boards does not require any protection circuit.
* when you are using more than one slaves device2 can "hear" and "respond" to the master's communication with device1 which is an disturbance.
* To overcome the disturbance,we use a protection circuit with pullup resistors(1kOhm ~10kOhm,Generally 4.7kOhm) on each the Slave Select line(CS).