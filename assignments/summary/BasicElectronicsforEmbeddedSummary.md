# Sensors and Actuators
![](https://www.avsystem.com/media/top_sensor_types_used_in_iot-02.png)
## Sensors

* A better term for a sensor is a transducer.
* A transducer is a physical device which converts one form of energy into another. The process of converting one form of energy to another is known as transduction.
* A sensor is type of transducer which converts some change in the physical envirnment into an electric signal which can easily used to take readings.
* For example:- a thirmistor is type of a sensor which changes its resistance when change in the temperature and that change in resistance can be easily converted into electrical signal.
* Not all transducers are sensors but most sensors are transducers. 
![](https://ae01.alicdn.com/kf/HTB1ZBQtRVXXXXbmXVXXq6xXFXXXK.jpg?size=485691&height=800&width=800&hash=80d46014834ba38f201c144d79e500bb)

## Actuators

* Actuator is also a type of a transducer and it is opposite of sensors.
* Actuators takes electrical input and convert them into a physical moving action.
* There are different types of Actuators like linear Actuator, rotary actuator,relays, valves etc.
* For example:- a motor is an example of an actuator it converts electrical energy into rotational motion.

![](https://i0.wp.com/3.212.229.64/wp-content/uploads/2020/04/dc_motors-1024x513.png?resize=771%2C386)

# Analog and Digital

## Analog

* An analog signal is a continuous signal that represents physical measurements.
* It is denoted by sine waves.
* It uses a continuous range of values that help you to represent information.
* It can be either periodic or non-periodic.
* For Example:- Human voice in air, analog electronic devices.

![](https://www.guru99.com/images/2/030720_0729_AnalogvsDig1.png)

## Digital

* Digital signals are time separated signals which are generated using digital modulation.
* It is denoted by square waves.
* Digital signal uses discrete 0 and 1 to represent information.
* Digital signal are continuous signals.
* For Example:- Computers, CDs, DVDs, and other digital electronic devices.

![](https://www.guru99.com/images/2/030720_0729_AnalogvsDig2.png)

![](https://i.pinimg.com/474x/1a/33/b4/1a33b4a4ceee864379523fb2b20ba47d.jpg)

# Micro controllers Vs Micro processors

## Microcontroller

* A microcontroller is an Integrated Circuit (IC) device which is optimised to control other electronic devices, performing a particular task and execute one specific application.
* It is specially designed circuits for embedded applications and is widely used in automatically controlled electronic devices.
* It contains memory, processor, and programmable I/O.
* Example:- Arduino, TI mSP430, AtTiny etc.

## Microprocessor

* Microprocessor is a controlling unit of a computer, fabricated on a small chip capable of performing ALU (Arithmetic Logical Unit) operations and communicating with the other devices connected to it.
* It has no RAM, ROM, Input-Output units, timers, and other peripherals on the chip, but can be connected externally.
Example:- Rasberry pi.

![](https://eeeproject.com/wp-content/uploads/2017/10/Microprocessor-vs-Microcontroller.jpg)

# Introduction to RPi

![](https://www.allaboutcircuits.com/uploads/articles/RPi_product_development_featured.jpeg)

* Raspberry Pi is a small single board computer. By connecting peripherals like Keyboard, mouse, display to the Raspberry Pi, it will act as a mini personal computer.
* Raspberry Pi is more than computer as it provides access to the on-chip hardware i.e. GPIOs for developing an application. By accessing GPIO, we can connect devices like sensors, actuators and can control them too.
* Rasberry pi is a ARM based microprocessor.
* We can install linux based operating systems like ubuntu, kali etc inside the Rasberry pi.

## Raspberry Pi Interfaces:

1. GPIO:- GPIO stands for General Purpose Input/Output. It’s a standard interface used to connect microcontrollers to other electronic devices like sensors, actuators etc.
2. UART:- A universal asynchronous receiver/transmitter (UART) is a block of circuitry responsible for implementing serial communication.The function of UART is to convert the incoming and outgoing data into the serial binary stream.
3. SPI:- The Serial Peripheral Interface (SPI) is a synchronous serial communication interface specification used for short-distance communication, primarily in embedded systems.
4. I2C:- I2C communication is the short form for inter-integrated circuits, used for the transfer of data between a central processor and multiple ICs on the same circuit board using just two common wires.
5. PWM:- Pulse Width Modulation, or PWM, is a technique for getting analog results with digital means.

# Serial and Parallel Communication

## Serial Communication

![](https://circuitglobe.com/wp-content/uploads/2019/07/serial-communication.jpg)

* In serial communication the data bits are transmitted serially over a common communication link one after the other.
* Basically it does not allow simultaneous transmission of data because only a single channel is utilized. Thereby allowing sequential transfer rather than simultaneous transfer.
* It is highly suitable for long distance signal transmission as only a single wire or bus is used. So, it can be connected between two points that are separated at a large distance with respect to each other.
* UART, SPI, I2C are exaples of serial communication.

## Parallel Communication

![](https://circuitglobe.com/wp-content/uploads/2019/07/parallel-communication.jpg)

* In parallel communication the various data bits are simultaneously transmitted using multiple communication links between sender and receiver.
* Here, despite using a single channel between sender and receiver, various links are used and each bit of data is transmitted separately over all the communication link.
* the transmission of 8-bit of data, 8 separate communication links are utilized. And so rather following a sequential data transmission, simultaneous transmission of data is allowed.This leads to a faster communication between the sender and receiver, but requires more data lines between sender and reciever.
* GPIO is an example of parallel communication.





